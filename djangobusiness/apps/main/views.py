from django.shortcuts import render
from django.http import *
from django.views.generic import View, TemplateView, UpdateView
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .forms import ExternalCreateUser, MainContact
from .models import CustomUser

from django.core.mail import send_mail

from django.core.exceptions import PermissionDenied
"""
Class based views https://docs.djangoproject.com/en/1.8/ref/class-based-views/
"""


class Home(View):
    def get(self, request):
        return render(request, 'main/home.html', {'user_pk': request.user.pk})


class Login(View):

    def get(self, request):
        return render(request,  'main/form_login.html')

    def post(self, request):
        logout(request)
        username = ''
        password = ''
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return HttpResponseRedirect('login/')


class Logout(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect('/')


class CreateAccount(View):
    def get(self, request):
        form = ExternalCreateUser()
        return render(request, 'main/create_account.html', {'form': form})

    def post(self, request):
        form = ExternalCreateUser(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
        else:
            return render(request, 'main/create_account.html', {'form': form})


class Profile(View):
    def get(self, request):
        return render(request, 'main/profile.html', {
            'user_pk': request.user.pk})


class UpdateAccount(UpdateView):
    model = CustomUser
    fields = ('tipo_usuario', 'first_name', 'last_name', 'password')
    template_name_suffix = '_update_form'

    def get_object(self, queryset=None):
        """ Hook to ensure object is owned by request.user. """
        obj = super(UpdateAccount, self).get_object()
        print(self.request.user.pk)
        if not obj.pk == self.request.user.pk:
            raise PermissionDenied
        return obj


class Contact(View):
    def get(self, request):
        form = MainContact()
        return render(request, 'main/contact.html', {'form': form})

    def post(self, request):
        form = MainContact(request.POST)
        if form.is_valid():
            message = 'Nome: ' + request.POST['nome'] + \
                '<br>Email: ' + request.POST.get('email') + \
                '<br>Telefone: ' + request.POST['telefone'] + \
                '<br> Mensagem: ' + request.POST['mensagem']
            print(settings.MAILGUN_MAIL)
            send_mail(
                'Contato via formulário', message, settings.MAILGUN_MAIL,
                ['fernando@segundoandar.com.br'],
                fail_silently=False, html_message=message)
            # Just redirect for some another url
            # return HttpResponseRedirect('/')
            form = MainContact()
        return render(request, 'main/contact.html', {'form': form})
