# from django.contrib.auth.forms import UserCreationForm, UserChangeForm
# from django.contrib.auth.models import User
#
# from .models import CustomUser, CustomUserManager
# from django import forms
# from django.contrib.auth.models import User
#
#
# class CustomUserCreationForm(UserCreationForm):
#     """
#     Form to create new users in admin
#     A form that creates a user, with no privileges, from the given email and
#     password.
#     """
#     def __init__(self, *args, **kargs):
#         super(CustomUserCreationForm, self).__init__(*args, **kargs)
#         # del self.fields['username']
#
#     class Meta:
#         model = CustomUser
#         fields = '__all__'
#
#
# class CustomUserChangeForm(UserChangeForm):
#     """A form for updating users. Includes all the fields on
#     the user, but replaces the password field with admin's
#     password hash display field.
#     """
#
#     def __init__(self, *args, **kargs):
#         super(CustomUserChangeForm, self).__init__(*args, **kargs)
#         # del self.fields['username']
#
#     class Meta:
#         model = CustomUser
#         fields = '__all__'
#
#
# class ExternalCreateUser(UserCreationForm, forms.Form):
#     TIPO_USUARIO_CHOICES = (
#             (2, 'Revenda'),
#             (3, 'Concessionária'),
#             (4, 'Particular'),
#         )
#
#     tipo_usuario = forms.ModelChoiceField(
#         queryset=TipoUsuario.objects.filter(pk__in=[2, 3, 4]))
#
#     class Meta:
#         model = CustomUser
#         fields = '__all__'
#         exclude = (
#             'is_staff', 'is_superuser', 'user_permissions', 'groups',
#             'last_login', 'created_at', 'date_joined')
#
#
# class MainContact(forms.Form):
#     nome = forms.CharField(max_length=100)
#     email = forms.EmailField(help_text="Por favor informe um email válido")
#     telefone = forms.CharField(max_length=20)
#     mensagem = forms.CharField(widget=forms.Textarea, max_length=500)
