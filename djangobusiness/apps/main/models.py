from django.db import models

# Create your models here.
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
import uuid

class Company(models.Model):
    razao_social = models.CharField(max_length=150)
    cnpj_cpf = models.CharField(
        max_length=40, null=True, blank=True, verbose_name="CPF/CNPJ")
    rg_ie = models.CharField(
        max_length=40, null=True, blank=True, verbose_name="RG/IE")
    country = models.CharField(max_length=100, null=True, blank=True, verbose_name="País")
    state = models.CharField(max_length=100, null=True, blank=True, verbose_name="UF")
    city = models.CharField(max_length=100, null=True, blank=True, verbose_name="Cidade")
    address = models.CharField(max_length=100, null=True, blank=True, verbose_name="Endereço")
    address_number = models.CharField(max_length=100, null=True, blank=True, verbose_name="Número")
    address_complement = models.CharField(max_length=100, null=True, blank=True, verbose_name="Complemento")
    address_postcode = models.CharField(max_length=100, null=True, blank=True, verbose_name="Cep")
    address_neighborhood = models.CharField(max_length=100, null=True, blank=True, verbose_name="Bairro")
    phone1 = models.CharField(max_length=100, null=True, blank=True, verbose_name="Telefone 1")
    phone2 = models.CharField(max_length=100, null=True, blank=True, verbose_name="Telefone 2")
    site = models.CharField(max_length=100, null=True, blank=True, verbose_name="Website")
    contact = models.CharField(max_length=100, null=True, blank=True, verbose_name="Contato")
    slug = models.SlugField(max_length=150)
    token = models.UUIDField(default=uuid.uuid4, editable=False)
    usuario_rel = models.ForeignKey(
        'CustomUser', default=None, related_name="empresa",
        on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Empresa"


class CustomUserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class CustomUser(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    full_name = models.CharField(_('full name'), max_length=30, blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    empresa = models.ForeignKey(
        Company, default=1, related_name="empresas",
        verbose_name="Empresa")

    # Campos compartilhados entre Revenda e Concessionaria
    cidade = models.CharField(max_length=200, null=True, blank=True)
    estado = models.CharField(max_length=100, null=True, blank=True)
    endereco = models.CharField(max_length=256, null=True, blank=True)
    telefone = models.CharField(max_length=40, null=True, blank=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_absolute_url(self):
        return "/editar-meus-dados/%s/" % self.pk

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])
