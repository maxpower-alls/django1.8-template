from django.apps import AppConfig

"""
Mais sobre possiveis configurações
https://docs.djangoproject.com/en/1.8/ref/applications/#application-configuration
"""


class MainConfig(AppConfig): # Our app config class
    name = 'cwb.apps.main'
    verbose_name = "Core da Aplicação"
