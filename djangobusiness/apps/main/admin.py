from django.contrib import admin

"""
# Register your models here.
from django.contrib.admin import AdminSite
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from .models import CustomUser, Company
from .forms import CustomUserChangeForm, CustomUserCreationForm
from django.contrib.auth.models import Group

class AdmConcessionaria(admin.StackedInline):
    prepopulated_fields = {"slug": ("nome",)}
    model = Concessionaria
    max_num = 1


class AdmRevenda(admin.StackedInline):
    prepopulated_fields = {"slug": ("nome",)}
    model = Revenda
    max_num = 1


class CustomUserAdmin(UserAdmin):
    inlines = [AdmRevenda, AdmConcessionaria]
    # The forms to add and change user instances
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Tipo de Conta'), {'fields': ('tipo_usuario',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Endereço'), {'fields': ('estado', 'cidade','endereco', 'telefone')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                                    'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            # 'fields': ('email', 'password1', 'password2', 'empresa')}
            'fields': ('email', 'password1', 'password2', 'tipo_usuario')}),
    )
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)



class AdmTipoUsuario(admin.ModelAdmin):
    pass

# Registra o novo tipo de usuario
admin.site.register(CustomUser, CustomUserAdmin)
# Remove os grupos de usuários do django
admin.site.unregister(Group)
admin.site.register(TipoUsuario, AdmTipoUsuario)
"""
