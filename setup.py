#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='djangobusiness',
    version='1.0',
    description="",
    author="Example Author",
    author_email='info@example.com',
    url='',
    packages=find_packages(),
    package_data={'djangobusiness': ['static/*.*', 'templates/*.*']},
    scripts=['manage.py'],
)
